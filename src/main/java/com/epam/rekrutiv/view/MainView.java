package com.epam.rekrutiv.view;

import java.util.Scanner;

import static com.epam.rekrutiv.controller.MainController.doActionForFirstChoice;
import static com.epam.rekrutiv.controller.MainController.doActionForSecondChoice;
import static com.epam.rekrutiv.model.Constant.*;

public class MainView {
    public static void loadMainMenu() {
        Scanner input = new Scanner(System.in);
        int choice;
        System.out.println(hello);
        do {
            System.out.println(messChoose);
            System.out.println(firstOption);
            System.out.println(secondOption);
            System.out.println(thirdOption);
            choice = input.nextInt();
            if (choice == 1) {
                doActionForFirstChoice();
            }
            else if (choice == 2) {
                doActionForSecondChoice();
            }
        } while (choice != 0);
    }
}
