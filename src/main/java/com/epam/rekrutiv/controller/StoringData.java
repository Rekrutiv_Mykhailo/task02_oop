package com.epam.rekrutiv.controller;

import com.epam.rekrutiv.model.Flower;
import com.epam.rekrutiv.model.Flowerpot;
import com.epam.rekrutiv.model.Season;

import java.util.ArrayList;
import java.util.List;

public class StoringData {
    public static List addFlowers() {
        List<Flower> flowers = new ArrayList<Flower>();
        flowers.add(new Flower("Rose", Season.ALL_YEAR, 10));
        flowers.add(new Flower("Orchid", Season.ALL_YEAR, 4100));
        flowers.add(new Flower("Tulip", Season.SPRING, 20));
        flowers.add(new Flower("Lily", Season.SUMMER, 30));
        flowers.add(new Flower("Gladiolus", Season.SPRING, 40));
        flowers.add(new Flower("Violet", Season.SUMMER, 50));
        return flowers;
    }
    public static List addFLowerPot(){
        List<Flowerpot>flowerpots=new ArrayList<Flowerpot>();
        flowerpots.add(new Flowerpot("Azalea",Season.ALL_YEAR,100,1,10));
        flowerpots.add(new Flowerpot("Areca",Season.ALL_YEAR,100,1,10));
        flowerpots.add(new Flowerpot("Bamboo",Season.ALL_YEAR,100,1,10));
        flowerpots.add(new Flowerpot("Begonia",Season.ALL_YEAR,100,1,10));
        flowerpots.add(new Flowerpot("Dracaena",Season.ALL_YEAR,100,1,10));
        flowerpots.add(new Flowerpot("Azalea",Season.ALL_YEAR,100,1,10));
        return flowerpots;
    }
}
