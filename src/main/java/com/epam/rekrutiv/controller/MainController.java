package com.epam.rekrutiv.controller;

import com.epam.rekrutiv.model.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import static com.epam.rekrutiv.model.Constant.*;
public class MainController {
    public static void doActionForFirstChoice() {
        Bouquet bouquet;
        List<ComponentOfBouget> componentOfBougets = new ArrayList<>();
        List<Flower> flowers = StoringData.addFlowers();
        Scanner scanner = new Scanner(System.in);
        int check;
        int numbOfFlower;
        int amountOfFlower;
        do {
            System.out.println(messOfCreatingBouget);
            for (int i = 0; i < flowers.size(); i++) {
                System.out.println(flowers.get(i) + "  choose: " + i);
            }
            System.out.println(messOfChoosingFlower);
            numbOfFlower = scanner.nextInt();
            System.out.println(messOfAmount);
            amountOfFlower = scanner.nextInt();
            componentOfBougets.add(new ComponentOfBouget(flowers.get(numbOfFlower), amountOfFlower));
            System.out.println(messOfContinueOrEnd);
            check = scanner.nextInt();
        } while (check == 1);
        bouquet = new Bouquet(componentOfBougets);
        System.out.println(bouquet);

    }

    public static void doActionForSecondChoice() {
        List<Flowerpot> flowerpots = StoringData.addFLowerPot();
        Scanner scanner = new Scanner(System.in);
        System.out.println(messOfChooseFlowerPot);
        for (int i = 0; i < flowerpots.size(); i++) {
            System.out.println(flowerpots.get(i).getName() + " type: " + i);
        }
        scanner.nextInt();
        System.out.println(messOfEnd);
    }
}
