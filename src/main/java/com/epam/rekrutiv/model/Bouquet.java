package com.epam.rekrutiv.model;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;

public class Bouquet {
    private List<ComponentOfBouget> componentOfBougets;

    public Bouquet() {
    }


    public Bouquet(List<ComponentOfBouget> componentOfBougets) {
       componentOfBougets.sort(new Comparator<ComponentOfBouget>() {
           @Override
           public int compare(ComponentOfBouget o1, ComponentOfBouget o2) {
               if(o1.getFlower().getPrice() == o2.getFlower().getPrice()){
                   return 0;
               }
               return (o1.getFlower().getPrice() >= o2.getFlower().getPrice()) ? -1 : 1;
           }
       });
       this.componentOfBougets=componentOfBougets;
    }


    public List<ComponentOfBouget> getComponentOfBougets() {
        return componentOfBougets;
    }

    public void setComponentOfBougets(List<ComponentOfBouget> component) {
        this.componentOfBougets = component;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Bouquet bouquet = (Bouquet) o;
        return Objects.equals(componentOfBougets, bouquet.componentOfBougets);
    }

    @Override
    public int hashCode() {
        return Objects.hash(componentOfBougets);
    }

    @Override
    public String toString() {
        return "Bouquet{" +
                "componentOfBougets=" + componentOfBougets +
                '}';
    }
}


