package com.epam.rekrutiv.model;

import java.util.Objects;

public class ComponentOfBouget {
    private Flower flower;
    private int amount;

    public ComponentOfBouget(Flower flower, int amount) {
        this.flower = flower;
        this.amount = amount;
    }

    public Flower getFlower() {
        return flower;
    }

    public void setFlower(Flower flower) {
        this.flower = flower;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ComponentOfBouget that = (ComponentOfBouget) o;
        return amount == that.amount &&
                Objects.equals(flower, that.flower);
    }

    @Override
    public int hashCode() {
        return Objects.hash(flower, amount);
    }

    @Override
    public String toString() {
        return "ComponentOfBouget{" +
                "flower=" + flower +
                ", amount=" + amount +
                '}';
    }
}
