package com.epam.rekrutiv.model;

public enum Season {
    WINTER, SPRING, SUMMER, AUTUMN, ALL_YEAR
}
