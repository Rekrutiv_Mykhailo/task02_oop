package com.epam.rekrutiv.model;

public class Constant {
    public static String messOfCreatingBouget = "let`s create your own bouget";
    public static String messOfChoosingFlower = "Choose flower:";
    public static String messOfAmount = "type amount of flower";
    public static String messOfContinueOrEnd = "Do you want add one more flower (type: 1)or finish with creating (type: 0)";
    public static String messOfChooseFlowerPot = "Please choose flowerpot";
    public static String messOfEnd = "Thank you for buying";
    public static String hello = "Hello!";
    public static String firstOption = "1. Do you want to buy bouget of flowers, enter 1";
    public static String secondOption ="2. Do you want to buy flowerpot, enter 2";
    public static String thirdOption = "3. Exit, enter 0";
    public static String messChoose = "Please choose the option";

}
