package com.epam.rekrutiv.model;

import java.util.Objects;

public class Flower {
    private String name;
    private Season season;
    private int price;

    public Flower(String name, Season season, int price) {
        this.name = name;
        this.season = season;
        this.price = price;

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Season getSeason() {
        return season;
    }

    public void setSeason(Season season) {
        this.season = season;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Flower flower = (Flower) o;
        return price == flower.price &&
                Objects.equals(name, flower.name) &&
                season == flower.season;
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, season, price);
    }

    @Override
    public String toString() {
        return "Flower{" +
                "name='" + name + '\'' +
                ", price=" + price +
                '}';
    }
}
