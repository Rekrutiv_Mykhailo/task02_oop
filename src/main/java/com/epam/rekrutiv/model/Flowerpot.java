package com.epam.rekrutiv.model;

import java.util.Objects;

public class Flowerpot extends Flower {
    private double high;
    private int ageOfLife;

    public Flowerpot(String name, Season season, int price, double high, int ageOfLife) {
        super(name, season, price);
        this.high = high;
        this.ageOfLife = ageOfLife;
    }

    public double getHigh() {
        return high;
    }

    public void setHigh(double high) {
        this.high = high;
    }

    public int getAgeOfLife() {
        return ageOfLife;
    }

    public void setAgeOfLife(int ageOfLife) {
        this.ageOfLife = ageOfLife;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        Flowerpot flowerpot = (Flowerpot) o;
        return Double.compare(flowerpot.high, high) == 0 &&
                ageOfLife == flowerpot.ageOfLife;
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), high, ageOfLife);
    }

    @Override
    public String toString() {
        return "Flowerpot{" +
                "high=" + high +
                ", ageOfLife=" + ageOfLife +
                '}';
    }
}
