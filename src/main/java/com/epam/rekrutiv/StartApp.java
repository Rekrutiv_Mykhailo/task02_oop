package com.epam.rekrutiv;

import com.epam.rekrutiv.view.MainView;

public class StartApp {
    public static void main(String[] args) {
        MainView.loadMainMenu();
    }
}
